import { Component, OnInit } from '@angular/core';
import {PurchaseService} from './purchase.service';

@Component({
  selector: 'app-purchases',
  templateUrl: './purchases.component.html',
  styleUrls: ['./purchases.component.css']
})
export class PurchasesComponent implements OnInit {

  constructor(private purchaseService: PurchaseService) { }

  ngOnInit() {
    this.refresh();
  }

  refresh() {
    this.purchaseService.refresh()
      .subscribe(
        data => {},
        error =>  console.log(`ERROR PurchasesComponent.refresh: ${JSON.stringify(error)}`)
      );
  }

  getNextPurchases() {
    this.purchaseService.getNextPurchases()
      .subscribe(
        data => console.log(`PurchasesComponent.getNextPurchases called`),
        error =>  console.log(`ERROR PurchasesComponent.getNextPurchases: ${JSON.stringify(error)}`)
      );
  }

}
