import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Article} from '../articles/article';
import {Purchase} from './purchase';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {UserService} from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class PurchaseService {
  private localServerUrl = '/api';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  purchases: Purchase[] = [];
  nextPage = 1;
  isMoreToShow = true;

  constructor(private http: HttpClient,
              private userService: UserService) { }

  refresh(): Observable<any> {
    this.nextPage = 1;
    this.isMoreToShow = true;

    const url = `${this.localServerUrl}/article/getpurchases`;
    const postBody = {page: this.nextPage};

    const httpOptions = {
      headers: new HttpHeaders({ Authorization: this.userService.getTokenHeader()})
    };

    return this.http.post<any>(url, postBody, httpOptions).pipe(
      tap(data => this.isMoreToShow = (data.next_page !== null)),
      map(data => data.purchases.map(
        purchase => new Purchase(purchase.id, purchase.article, purchase.buyer, purchase.purchase_at)
      )),
      tap((data) => {
          this.purchases = data;
          this.nextPage += 1;
        },
        error => console.log(`ERROR PurchaseService.refresh: ${error.message}`))
    );
  }

  getNextPurchases(): Observable<Purchase[]> {
    const url = `${this.localServerUrl}/article/getpurchases`;
    const postBody = {page: this.nextPage};

    const httpOptions = {
      headers: new HttpHeaders({ Authorization: this.userService.getTokenHeader()})
    };

    return this.http.post<any>(url, postBody, httpOptions).pipe(
      tap(data => {
        this.isMoreToShow = (data.next_page !== null);
        console.log(`PurchaseService.getNextPurchases: ${JSON.stringify(data)} ${this.isMoreToShow}`);
      }),
      map(data => data.purchases.map(
        purchase => new Purchase(purchase.id, purchase.article, purchase.buyer, purchase.purchase_at)
      )),
      tap((data) => {
          this.purchases = this.purchases.concat(data);
          this.nextPage += 1;
        },
        error => console.log(`ERROR PurchaseService.getNextPurchases: ${error.message}`))
    );
  }
}
