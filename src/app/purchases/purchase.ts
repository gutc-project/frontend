import {User} from '../user/user';
import {Article} from '../articles/article';

export class Purchase {
  id: number;
  buyer: User;
  article: Article;
  purchaseAt: Date;


  constructor(id: number, article: any, buyer: any, purchaseAt: string) {

    this.id = id;
    this.buyer = new User(buyer.id, buyer.pseudo, buyer.email, buyer.public_key, buyer.last_connection);
    this.article = new Article(article.id, article.name, article.price, article.description,
      article.author.pseudo, article.author.public_key);
    this.purchaseAt = new Date(purchaseAt);
  }
}
