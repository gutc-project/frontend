import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExploreArticlesComponent } from './explore-articles.component';

describe('ExploreArticlesComponent', () => {
  let component: ExploreArticlesComponent;
  let fixture: ComponentFixture<ExploreArticlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExploreArticlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExploreArticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
