import {Component, Injectable, OnInit} from '@angular/core';
import {
  AbstractControl,
  AsyncValidator, AsyncValidatorFn,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms';
import {User} from '../user/user';
import {UserService} from '../user/user.service';
import {MatSnackBar} from '@angular/material';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({ providedIn: 'root' })
export class UniquePseudoValidator implements AsyncValidator {
  user: User = null;

  constructor(private userService: UserService) {}

  public setUser(user: User) {
    this.user = user;
  }

  validate(ctrl: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    if (this.user && this.user.pseudo !== ctrl.value ) {
      return this.userService.isPseudoTaken(ctrl.value).pipe(
        map(isTaken => {
          return isTaken ? {uniquePseudo: true} : null;
        }),
        catchError(() => null)
      );
    }
    return of(null);
  }
}

@Injectable({ providedIn: 'root' })
export class UniqueEmailValidator implements AsyncValidator {
  user: User = null;

  constructor(private userService: UserService) {}

  public setUser(user: User) {
    this.user = user;
  }

  validate(ctrl: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    if (this.user && this.user.email !== ctrl.value ) {
      return this.userService.isEmailTaken(ctrl.value).pipe(
        map(isTaken => {
          return isTaken ? {uniqueEmail: true} : null;
        }),
        catchError(() => null)
      );
    }
    return of(null);
  }
}

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {

  user: User = null;

  pseudoUsiqueValidator = new UniquePseudoValidator(this.userService);
  emailUniqueValidator = new UniqueEmailValidator(this.userService);

  userForm = this.fb.group({
    pseudo: ['', [Validators.required], [this.pseudoUsiqueValidator]],
    email: ['', [Validators.required], [this.emailUniqueValidator]],
    publicKey: ['']
  }, {updateOn: 'change'});

  constructor(
    private userService: UserService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.userService.getUserInfo().subscribe(
      data => {
        this.user = data;
        console.log(`UserInfoComponent: ${JSON.stringify(this.user)}`);
        this.updateFormData();
        this.emailUniqueValidator.setUser(this.user);
        this.pseudoUsiqueValidator.setUser(this.user);
      },
      error => this.snackBar.open('ohoh... quelque chose c\'est mal passé', '' , {duration: 2000})
    );
  }

  public getUser(): User {
    return this.user;
  }

  private updateFormData() {
    this.userForm.setValue({
      pseudo: this.user.pseudo,
      email: this.user.email,
      publicKey: this.user.publicKey !== 'none' ? this.user.publicKey : ''
    });
  }

  onSubmit() {
    if (this.validForm()) {
      this.userService.updateUserInfo(
        this.userForm.controls.pseudo.value,
        this.userForm.controls.email.value,
        this.userForm.controls.publicKey.value).subscribe(
          data => {
            this.snackBar.open('Vos informations ont été mise à jour', '' , {duration: 2000});
            this.router.navigate(['/explore']);
          },
        error => this.snackBar.open('ohoh... quelque chose c\'est mal passé', '' , {duration: 2000})
      );
    }
  }

  validForm() {
    if (!this.user) {
      return false;
    }

    const valid: boolean = this.userForm.valid;
    const modified: boolean = this.userForm.controls.pseudo.value !== this.user.pseudo ||
                              this.userForm.controls.email.value !== this.user.email ||
                              (this.userForm.controls.publicKey.value !== '' &&
                                  this.userForm.controls.publicKey.value !== this.user.publicKey);

    console.warn(valid, modified);

    return valid && modified;
  }

  get f() { return this.userForm.controls; }
}
