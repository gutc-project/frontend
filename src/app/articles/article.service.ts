import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { UserService } from '../user/user.service';

import { Article } from './article';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  private localServerUrl = '/api';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  articles: Article[] = [];
  nextPage = 1;
  isMoreToShow = true;

  constructor(private http: HttpClient, private userService: UserService) { }

  refresh(): Observable<any> {
    this.nextPage = 1;
    this.isMoreToShow = true;

    const url = `${this.localServerUrl}/article/get`;
    const postBody = {page: this.nextPage};

    return this.http.post<any>(url, postBody, this.httpOptions).pipe(
      tap(data => this.isMoreToShow = (data.next_page !== null)),
      map(data => data.articles.map(
        article => new Article(article.id, article.name, article.price, article.description,
                               article.author.pseudo, article.author.public_key)
      )),
      tap((data) => {
        this.articles = data;
        this.nextPage += 1;
        },
        error => console.log(`ERROR ArticleService.refresh: ${error.message}`))
    );
  }

  getNextArticles(): Observable<Article[]> {
    const url = `${this.localServerUrl}/article/get`;
    const postBody = {page: this.nextPage};


    return this.http.post<any>(url, postBody, this.httpOptions).pipe(
      tap(data => this.isMoreToShow = (data.next_page !== null)),
      map(data => data.articles.map(
        article => new Article(article.id, article.name, article.price, article.description,
                               article.author.pseudo, article.author.public_key)
      )),
      tap((data) => {
        this.articles = this.articles.concat(data);
        this.nextPage += 1;
        },
          error => console.log(`ERROR ArticleService.getNextArticles: ${error.message}`))
    );
  }

  uploadNewArticle(name: string, price: number, description: string, image: File) {
    const url = `${this.localServerUrl}/article/upload`;

    const fd = new FormData();
    fd.append('name', name);
    fd.append('price', String(price));
    fd.append('description', description);
    fd.append('image', image, image.name);

    console.log(`formdata: ${String(price)}`);

    const httpOptions = {
      headers: new HttpHeaders({ Authorization: this.userService.getTokenHeader()})
    };

    return this.http.post<any>(url, fd, httpOptions).pipe(
      tap((data) => {
        const newArticle = new Article(data.article.id, data.article.name, data.article.price,
                                       data.article.description, data.article.author.pseudo,
                                       data.article.author.public_key);
        this.articles.unshift(newArticle);
        },
          error => console.log(`ERROR ArticleService.uploadNewArticle: ${JSON.stringify(error)}`))
    );
  }

  delete(id: number) {
    const url = `${this.localServerUrl}/article/delete`;
    const postBody = {id};

    const httpOptions = {
      headers: new HttpHeaders({ Authorization: this.userService.getTokenHeader()})
    };

    return this.http.post<any>(url, postBody, httpOptions).pipe(
      tap((data) => {
          this.articles = this.articles.filter(item => item.id !== id);
        },
        error => console.log(`ERROR ArticleService.delete: ${JSON.stringify(error)}`))
    );
  }

  purchase(id: string, token: string) {
    const url = `${this.localServerUrl}/article/purchase/${id}/${token}`;

    return this.http.get<any>(url, {}).pipe(
      tap((data) => {},
        error => console.log(`ERROR ArticleService.purchase: ${JSON.stringify(error)}`))
    );
  }
}
