import { Component, OnInit } from '@angular/core';
import { Article } from './article';
import { ArticleService } from './article.service';
import {MatSnackBar} from '@angular/material';
import {useAnimation} from '@angular/animations';
import {UserService} from '../user/user.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {
  constructor(private articleService: ArticleService,
              private userService: UserService,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.refresh();
  }

  refresh() {
    this.articleService.refresh()
      .subscribe(
        data => {},
        error =>  console.log(`ERROR ArticleComponent.refresh: ${JSON.stringify(error)}`)
      );
  }

  getNextArticles() {
    this.articleService.getNextArticles()
      .subscribe(
        data => {},
        error =>  console.log(`ERROR ArticleComponent.getNextArticles: ${JSON.stringify(error)}`)
      );
  }

  delete(id: number) {
    this.articleService.delete(id).subscribe(
      data => this.snackBar.open('l\'article est supprimé', '', {duration: 2000}),
      error => this.snackBar.open('ohoh... quelque chose c\'est mal passé', '' , {duration: 2000})
    );
  }

  isEditable(authorPseudo: string) {
    return this.userService.isLoggedIn() && this.userService.getPseudo() === authorPseudo;
  }

  purchase(article) {
    this.userService.getPurchaseToken().subscribe(
      token => {
        const cesiumUrl = article.getCesiumLink(token);
        window.location.href = cesiumUrl;
      },
      error => this.snackBar.open('ohoh... quelque chose c\'est mal passé', '', {duration: 2000})
    );
  }
}
