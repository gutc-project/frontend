export class Article {
  id: number;
  name: string;
  price: number;
  description: string;
  authorPseudo: string;
  authorPublicKey: string;

  constructor(id: number, name: string, price: number, description: string,
              authorPseudo: string, authorPublicKey: string) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.description = description;
    this.authorPseudo = authorPseudo;
    this.authorPublicKey = authorPublicKey;
  }

  getSrc() {
    return `api/article/image?id=${this.id}`;
  }

  getCesiumLink(userToken) {
    const pubkey = encodeURIComponent(this.authorPublicKey);
    const amount = encodeURIComponent(this.price);
    const comment = encodeURIComponent(this.id);
    const name = encodeURIComponent('UTChange');
    // tslint:disable-next-line:variable-name
    const redirect_url = encodeURIComponent(`http://localhost:4200/purchase/${this.id}/${userToken}`);
    // tslint:disable-next-line:variable-name
    const cancel_url = encodeURIComponent('http://localhost:4200');


    return `https://g1.duniter.fr/api/#/v1/payment/${pubkey}?amount=${amount}&comment=${comment}&name=${name}&redirect_url=${redirect_url}&cancel_url=${cancel_url}`;

    // console.log(`http://localhost:4200/purchase/${this.id}/${userToken}`);
    // return 'localhost:4200';
  }
}
