import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar} from '@angular/material';

import { UserService } from '../user/user.service';
import {ArticleService} from '../articles/article.service';

export interface DialogData {
  name: string;
  price: number;
  description: string;
  image: File;
}

@Component({
  selector: 'app-upload-article',
  templateUrl: './upload-article.component.html',
  styleUrls: ['./upload-article.component.css']
})
export class UploadArticleComponent implements OnInit {

  constructor(public dialog: MatDialog,
              private snackBar: MatSnackBar,
              private userService: UserService,
              private articleService: ArticleService) { }

  ngOnInit() {
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(DialogUploadArticleComponent, {
      data: {name: null, price: null, description: null, image: null}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`The upload dialog was closed: ${JSON.stringify(result)}`);
      console.log(result.image);
      if (result && result.name && result.price && result.description && result.image) {
        console.log('calling articleService');

        this.articleService.uploadNewArticle(result.name, result.price, result.description, result.image)
          .subscribe(
            data => this.snackBar.open('Le nouvel article est en ligne !', '' , {duration: 2000}),
            error => this.snackBar.open('ohoh... quelque chose c\'est mal passé', '' , {duration: 2000})
          );
      }
    });
  }

  addButtonClicked() {
    if (this.userService.isLoggedOut()) {
      this.snackBar.open('Vous devez vous indentifier pour ajouter un article ! ', '' , {duration: 2000});
    } else {
      this.openDialog();
    }
  }
}

@Component({
  selector: 'app-dialog-upload-article',
  templateUrl: './dialog-upload-article.component.html',
  styleUrls: ['./upload-article.component.css']
})
export class DialogUploadArticleComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogUploadArticleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onFileSelected(event) {
    console.log(event);
    this.data.image = event.target.files[0];
    console.log(this.data.image);
  }
}
