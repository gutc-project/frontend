import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatCardModule, MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule, MatListModule, MatRadioModule,
  MatSelectModule, MatSidenavModule, MatSnackBarModule,
  MatToolbarModule, MatTooltipModule
} from '@angular/material';

const materials = [
  MatButtonModule,
  MatToolbarModule,
  MatIconModule,
  MatExpansionModule,
  MatDialogModule,
  MatFormFieldModule,
  MatSelectModule,
  MatInputModule,
  MatSnackBarModule,
  MatTooltipModule,
  MatCardModule,
  MatRadioModule,
  MatDatepickerModule,
  MatSidenavModule,
  MatListModule
];

@NgModule({
  imports: [materials],
  exports: [materials]
})
export class MaterialModule { }
