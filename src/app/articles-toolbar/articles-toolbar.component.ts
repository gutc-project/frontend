import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user.service';
import { ArticleService } from '../articles/article.service';
import {MatSnackBar} from '@angular/material';


@Component({
  selector: 'app-articles-toolbar',
  templateUrl: './articles-toolbar.component.html',
  styleUrls: ['./articles-toolbar.component.css']
})
export class ArticlesToolbarComponent implements OnInit {

  constructor(private userService: UserService,
              private articleService: ArticleService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
  }

  refresh() {
    this.articleService.refresh().subscribe(
      data => {},
      error => console.log(`ERROR ArticlesToolbarComponent.refresh: ${JSON.stringify(error)}`)
    );
  }
}
