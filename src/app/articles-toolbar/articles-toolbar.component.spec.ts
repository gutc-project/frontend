import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesToolbarComponent } from './articles-toolbar.component';

describe('ArticlesToolbarComponent', () => {
  let component: ArticlesToolbarComponent;
  let fixture: ComponentFixture<ArticlesToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticlesToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
