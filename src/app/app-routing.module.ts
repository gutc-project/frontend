import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserInfoComponent} from './user-info/user-info.component';
import {ExploreArticlesComponent} from './explore-articles/explore-articles.component';
import {RegisterComponent} from './register/register.component';
import {PurchaseProcessingComponent} from './purchase-processing/purchase-processing.component';
import {PurchasesComponent} from './purchases/purchases.component';


const routes: Routes = [
  { path: 'user', component: UserInfoComponent},
  { path: 'explore', component: ExploreArticlesComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'purchase/:id/:token', component: PurchaseProcessingComponent},
  { path: 'purchase', component: PurchasesComponent},
  { path: '', redirectTo: '/explore', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
