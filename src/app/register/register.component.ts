import {Component, Inject, Injectable, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material';

import { UserService } from '../user/user.service';
import {
  AbstractControl, AsyncValidator,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators
} from '@angular/forms';
import {Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Router} from '@angular/router';


export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}

@Injectable({ providedIn: 'root' })
export class UniquePseudoValidator implements AsyncValidator {
  constructor(private userService: UserService) {}

  validate(
    ctrl: AbstractControl
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return this.userService.isPseudoTaken(ctrl.value).pipe(
      map(isTaken => {
        return isTaken ? { uniquePseudo: true } : null;
      }),
      catchError(() => null)
    );
  }
}

@Injectable({ providedIn: 'root' })
export class UniqueEmailValidator implements AsyncValidator {
  constructor(private userService: UserService) {}

  validate(
    ctrl: AbstractControl
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return this.userService.isEmailTaken(ctrl.value).pipe(
      map(isTaken => {
        return isTaken ? { uniqueEmail: true } : null;
      }),
      catchError(() => null)
    );
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  minPW = 5;
  hidePassword = true;
  hideConfirmPassword = true;

  registerForm = this.fb.group({
    pseudo: ['', [Validators.required], [new UniquePseudoValidator(this.userService)]],
    email: ['', [Validators.required, Validators.email], [new UniqueEmailValidator((this.userService))]],
    password: ['', [Validators.required, Validators.minLength(5)]],
    confirmPassword: ['', [Validators.required]]
  }, {validator: MustMatch('password', 'confirmPassword'),
    updateOn: 'blur'});

  constructor(private snackBar: MatSnackBar,
              private userService: UserService,
              private fb: FormBuilder,
              private router: Router) { }


  onSubmit() {
    if (this.registerForm.valid) {
      this.userService.register(this.f.pseudo.value, this.f.email.value, this.f.password.value).subscribe(
        data => {
          this.snackBar.open('Tu peux maintenant t\'identifier', '' , {duration: 2000});
          this.router.navigate(['/explore']);
        },
        error => this.snackBar.open('ohoh... quelque chose c\'est mal passé', '' , {duration: 2000})
      );
    }
  }

  get f() { return this.registerForm.controls; }
}
