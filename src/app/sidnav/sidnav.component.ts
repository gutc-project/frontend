import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import {SidenavService} from './sidenav.service';
import {MatSidenav} from '@angular/material';
import {UserService} from '../user/user.service';

@Component({
  selector: 'app-sidnav',
  templateUrl: './sidnav.component.html',
  styleUrls: ['./sidnav.component.css']
})
export class SidnavComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;
  @ViewChild('sidenav', {static: true}) public sidenav: MatSidenav;

  // tslint:disable-next-line:variable-name
  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef,
              media: MediaMatcher,
              private sidenavService: SidenavService,
              private userService: UserService) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  ngOnInit() {
    console.warn(`hey: ${this.sidenav}`);
    this.sidenavService.setSidenav(this.sidenav);
    if (!this.mobileQuery.matches) {
      this.sidenavService.toggle();
    }
  }

}
