import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserService} from '../user/user.service';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MailService {
  private localServerUrl = '/api';

  constructor(private http: HttpClient,
              private userService: UserService) { }

  public sendMail(textBody: string, articleId: number) {
    const url = `${this.localServerUrl}/message/sendmail`;
    const postBody = {text_body: textBody, article_id: articleId};

    const httpOptions = {
      headers: new HttpHeaders({ Authorization: this.userService.getTokenHeader()})
    };

    return this.http.post<any>(url, postBody, httpOptions).pipe(
      tap(
        data => {},
        error => console.log(`ERROR MailSender.sendMail : ${JSON.stringify(error)}`)
      )
    );
  }
}
