import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {ArticleService} from '../articles/article.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-purchase-processing',
  templateUrl: './purchase-processing.component.html',
  styleUrls: ['./purchase-processing.component.css']
})
export class PurchaseProcessingComponent implements OnInit {

  constructor(private router: Router,
              private route: ActivatedRoute,
              private articleService: ArticleService,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    const token = this.route.snapshot.paramMap.get('token');

    this.articleService.purchase(id, token).subscribe(
      data => {
        this.snackBar.open('votre achat a été pris en compte', '', {duration: 2000});
        this.router.navigate(['/explore']);
      },
      error => this.snackBar.open('ohoh... quelque chose c\'est mal passé', '' , {duration: 2000})
    );
  }

}
