import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseProcessingComponent } from './purchase-processing.component';

describe('PurchaseProcessingComponent', () => {
  let component: PurchaseProcessingComponent;
  let fixture: ComponentFixture<PurchaseProcessingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseProcessingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseProcessingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
