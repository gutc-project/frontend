import {Component, Inject, Input} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {DialogData, DialogLoginComponent} from '../login/login.component';
import {MailService} from '../mail/mail.service';

export interface DialogData {
  message: string;
}

@Component({
  selector: 'app-mail-sender',
  templateUrl: './mail-sender.component.html',
  styleUrls: ['./mail-sender.component.css']
})
export class MailSenderComponent {

  @Input() articleId: number;

  constructor(public dialog: MatDialog,
              private snackBar: MatSnackBar,
              private mailService: MailService) {}


  public openDialog(): void {
    const dialogRef = this.dialog.open(DialogMailSenderComponent, {
      height: '300px',
      width: '500px',
      data: {message: 'Bonjour,\n\nJe suis intéressé par ton article posté sur UTChange.\n\nPeux-tu me recontacter ?\nMerci !'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.message) {
        this.mailService.sendMail(result.message, this.articleId).subscribe(
          data => this.snackBar.open('message envoyé', '' , {duration: 2000}),
          error => this.snackBar.open('ohoh... quelque chose c\'est mal passé', '' , {duration: 2000})
        );
      }
    });
  }
}

@Component({
  selector: 'app-dialog-mail-sender',
  templateUrl: './dialog-mail-sender.component.html',
  styleUrls: ['./mail-sender.component.css']
})
export class DialogMailSenderComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogLoginComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

}
