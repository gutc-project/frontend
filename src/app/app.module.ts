import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from './material/material.module';

import { ArticlesComponent } from './articles/articles.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ArticlesToolbarComponent } from './articles-toolbar/articles-toolbar.component';
import {HttpClientModule} from '@angular/common/http';
import {LoginComponent, DialogLoginComponent} from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DialogUploadArticleComponent, UploadArticleComponent} from './upload-article/upload-article.component';
import {DialogMailSenderComponent, MailSenderComponent} from './mail-sender/mail-sender.component';
import { RegisterComponent} from './register/register.component';
import { AppRoutingModule } from './app-routing.module';
import { UserInfoComponent } from './user-info/user-info.component';
import { ExploreArticlesComponent } from './explore-articles/explore-articles.component';
import {SidnavComponent} from './sidnav/sidnav.component';
import {SidenavService} from './sidnav/sidenav.service';
import { PurchaseProcessingComponent } from './purchase-processing/purchase-processing.component';
import { PurchasesComponent } from './purchases/purchases.component';

@NgModule({
  declarations: [
    AppComponent,
    ArticlesComponent,
    FooterComponent,
    HeaderComponent,
    ArticlesToolbarComponent,
    LoginComponent,
    DialogLoginComponent,
    UploadArticleComponent,
    DialogUploadArticleComponent,
    MailSenderComponent,
    DialogMailSenderComponent,
    RegisterComponent,
    UserInfoComponent,
    ExploreArticlesComponent,
    SidnavComponent,
    PurchaseProcessingComponent,
    PurchasesComponent
  ],
  entryComponents: [
    DialogLoginComponent,
    DialogUploadArticleComponent,
    DialogMailSenderComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
