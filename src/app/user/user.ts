export class User {
  id: string;
  pseudo: string;
  email: string;
  publicKey: string;
  lastConnection: string[];


  constructor(id: string, pseudo: string, email: string, publicKey: string, lastConnection: string[]) {
    this.id = id;
    this.pseudo = pseudo;
    this.email = email;
    this.publicKey = publicKey;
    this.lastConnection = lastConnection;
  }
}
