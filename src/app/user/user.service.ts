import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map, tap} from 'rxjs/operators';
import * as moment from 'moment';
import {Observable, of} from 'rxjs';
import {User} from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private localServerUrl = '/api';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  public login(pseudo: string, password: string) {
    return this.http.post<any>(`${this.localServerUrl}/auth/signin`, {pseudo, password}, this.httpOptions)
      .pipe(
        tap((res) => {
        console.log(`login: ${JSON.stringify(res)}`);
        this.setSession(res);
        },
        error => console.log(`ERROR UserService.login: ${JSON.stringify(error.error)}`)
      ));
  }

  public register(pseudo: string, email: string, password: string) {
    return this.http.post<any>(`${this.localServerUrl}/auth/signup`, {pseudo, email, password}, this.httpOptions)
      .pipe(
        tap((res) => {
            console.log(`register: ${JSON.stringify(res)}`);
          },
          error => console.log(`ERROR UserService.register: ${JSON.stringify(error.error.msg)}`)
        ));
  }

  public isPseudoTaken(pseudo: string) {
    return this.http.post<any>(`${this.localServerUrl}/auth/ispseudoavailable`, {pseudo}, this.httpOptions)
      .pipe(
        tap((res) => {
            console.log(`isPseudoTaken: ${JSON.stringify(res)}`);
          },
          error => {
            console.log(`ERROR UserService.isPseudoTaken: ${JSON.stringify(error)}`);
          }
        ),
        map(data => data.pseudo !== 'available')
      );
  }

  public isEmailTaken(email: string) {
    return this.http.post<any>(`${this.localServerUrl}/auth/isemailavailable`, {email}, this.httpOptions)
      .pipe(
        tap((res) => {
            console.log(`isEmailTaken: ${JSON.stringify(res)}`);
          },
          error => {
            console.log(`ERROR UserService.isEmailTaken: ${JSON.stringify(error)}`);
          }
        ),
        map(data => data.email !== 'available')
      );
  }

  public getUserInfo(): Observable<User> {
    const httpOptions = {
      headers: new HttpHeaders({ Authorization: this.getTokenHeader()})
    };

    return this.http.post<any>(`${this.localServerUrl}/user/getuserinfo`, {}, httpOptions)
      .pipe(
        tap((res) => console.log(`getUserInfo: ${JSON.stringify(res)}`),
          (error) => console.log(`ERROR getUserInfo: ${JSON.stringify(error)}`)),
        map(data => new User(data.user.id, data.user.pseudo, data.user.email, data.user.public_key, data.user.last_connection))
      );
  }

  public updateUserInfo(newPseudo: string, newEmail: string, newPublicKey: string) {
    const httpOptions = {
      headers: new HttpHeaders({ Authorization: this.getTokenHeader()})
    };

    const body = {pseudo: newPseudo, email: newEmail, public_key: newPublicKey};

    return this.http.post<any>(`${this.localServerUrl}/user/updateuserinfo`, body, httpOptions)
      .pipe(
        tap((res) => console.log(`updateUserInfo: ${JSON.stringify(res)}`),
          (error) => console.log(`ERROR updateUserInfo: ${JSON.stringify(error)}`))
      );
  }

  public getPseudo(): string {
    return JSON.parse(atob(this.getToken().split('.')[1])).sub;
  }

  private setSession(authResult) {
    const token = authResult.access_token;
    const tokenDecode = JSON.parse(atob(token.split('.')[1]));
    const tokenExp = tokenDecode.exp;

    const expiresAt = moment().add(tokenExp, 'second');

    localStorage.setItem('token', token);
    localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('expires_at');
  }

  public isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getToken() {
    return localStorage.getItem('token');
  }

  getExpiration() {
    const expiration = localStorage.getItem('expires_at');
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }

  getTokenHeader() {
    return `Bearer ${this.getToken()}`;
  }

  getPurchaseToken() {
    const httpOptions = {
      headers: new HttpHeaders({ Authorization: this.getTokenHeader()})
    };

    return this.http.post<any>(`${this.localServerUrl}/article/getpurchasetoken`, {}, httpOptions)
      .pipe(
        tap((res) => console.log(`getPurchaseToken: ${JSON.stringify(res)}`),
          (error) => console.log(`ERROR getPurchaseToken: ${JSON.stringify(error)}`)),
        map(data => data.token)
      );
  }
}
