import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar} from '@angular/material';

import { UserService } from '../user/user.service';
import {Router} from '@angular/router';
import {ArticleService} from '../articles/article.service';
import {PurchaseService} from '../purchases/purchase.service';

export interface DialogData {
  pseudo: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  constructor(public dialog: MatDialog,
              private snackBar: MatSnackBar,
              private userService: UserService,
              private articleService: ArticleService,
              private purchaseService: PurchaseService,
              private router: Router) {
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(DialogLoginComponent, {
      data: {pseudo: '', password: ''}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.userService.login(result.pseudo, result.password).subscribe(
          data => this.snackBar.open('Bienvenue !', '' , {duration: 2000}),
          error => this.snackBar.open('ohoh... quelque chose c\'est mal passé', '' , {duration: 2000})
        );
      }
    });
  }

  logout() {
    this.userService.logout();

    this.articleService.refresh().subscribe(
      data => {},
      error =>  console.log(`ERROR LoginComponent.logout: ${JSON.stringify(error)}`)
    );

    this.purchaseService.refresh().subscribe(
      data => {},
      error =>  console.log(`ERROR LoginComponent.logout: ${JSON.stringify(error)}`)
    );

    this.router.navigate(['/explore']);
  }
}

@Component({
  selector: 'app-login-component-dialog',
  templateUrl: './login.component.dialog.html',
  styleUrls: ['./login.component.css']
})
export class DialogLoginComponent {
  hide = true;

  constructor(
    public dialogRef: MatDialogRef<DialogLoginComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

}
